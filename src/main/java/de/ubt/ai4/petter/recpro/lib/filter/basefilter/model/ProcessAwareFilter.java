package de.ubt.ai4.petter.recpro.lib.filter.basefilter.model;

import de.ubt.ai4.petter.recpro.lib.attribute.modeling.model.RecproAttribute;
import de.ubt.ai4.petter.recpro.lib.bpm.model.modeling.BpmElement;
import de.ubt.ai4.petter.recpro.lib.bpm.model.modeling.User;
import de.ubt.ai4.petter.recpro.lib.rating.rating.modeling.model.Rating;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
public class ProcessAwareFilter extends Filter {
    private List<BpmElement> inputElements = new ArrayList<>();
    private List<RecproAttribute> inputAttributes = new ArrayList<>();
    private List<Rating> inputRatings = new ArrayList<>();
    private List<User> inputUsers = new ArrayList<>();

    private boolean allInputElements = false;
    private boolean allInputAttributes = false;
    private boolean allInputRatings = false;
    private boolean allInputUsers = false;
    private int traceLength;
}
