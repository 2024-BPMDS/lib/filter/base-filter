package de.ubt.ai4.petter.recpro.lib.filter.basefilter.model;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

@JsonTypeInfo(
        include = JsonTypeInfo.As.EXISTING_PROPERTY,
        use = JsonTypeInfo.Id.NAME,
        property = "filterType",
        visible = true)
@JsonSubTypes({
        @JsonSubTypes.Type(value = KnowledgeBasedFilter.class, name = "KNOWLEDGE_BASED"),
        @JsonSubTypes.Type(value = CollaborativeFilter.class, name = "COLLABORATIVE"),
        @JsonSubTypes.Type(value = ContentBasedFilter.class, name = "CONTENT_BASED"),
        @JsonSubTypes.Type(value = HybridFilter.class, name = "HYBRID"),
        @JsonSubTypes.Type(value = ProcessAwareFilter.class, name = "PROCESS_AWARE"),
        @JsonSubTypes.Type(value = BaseFilter.class, name = "BASE"),

})

public class Filter {
    private String id;
    private String name;
    private String description;
    private String filterUrl;
    private FilterType filterType;
}
