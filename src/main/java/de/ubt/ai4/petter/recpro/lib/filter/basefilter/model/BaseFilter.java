package de.ubt.ai4.petter.recpro.lib.filter.basefilter.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class BaseFilter extends Filter {
    private boolean ascending;
    private TasklistOrder tasklistOrder;
}
